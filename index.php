<?php
require 'vendor/autoload.php';
require 'Mybot.php';
use PhpSlackBot\Bot;

$bot = new Bot();
$bot->setToken('YOUR SLACK TOKEN KEY'); // Get your token here https://my.slack.com/services/new/bot
$bot->loadCommand(new MyBot());
$bot->run();