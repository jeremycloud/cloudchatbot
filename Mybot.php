<?php
require 'vendor/autoload.php';
include 'CloudwaysAPIClient.php';
use PhpSlackBot\Bot;
// Custom command
class MyBot extends \PhpSlackBot\Command\BaseCommand {
    protected function configure() {
        $this->setName("cloud");
    }
    protected function execute($message, $context) {
        $args = $this->getArgs($message);
        $command = isset($args[1]) ? $args[1] : '';
        $api_key = 'YOUR CLOUDWAYS API KEY';
        $email = 'YOUR CLOUDWAYS EMAIL';
        $cw_api = new CloudwaysAPIClient($email,$api_key);
        switch ($command) {
            case "getservers":
            $servers = $cw_api->get_servers();
            $msg = null;
            foreach($servers->servers as $server) {
                $msg = "ServerID: ".$server->id." Name:".$server->label." Status: " .$server->status ." Total Apps: " . sizeof($server->apps) . "\n";                
            }
            $this->send($this->getCurrentChannel(), null, $msg);
            break;
            case "getapplication":
            $serverid = isset($args[2]) ? $args[2] : '';
            $servers = $cw_api->get_servers();
            if(sizeof($args) == 3){
                if($serverid === "all"){
                    foreach($servers->servers as $server) {
                        foreach ($server->apps as $app) {
                            if(empty($msg))
                            {
                                $msg = "Server: ".$server->label." ApplicationID:".$app->id." ApplicationLabel:".$app->label. "\n";
                            }
                            $msg .= "Server: ".$server->label." ApplicationID:".$app->id." ApplicationLabel:".$app->label. "\n";
                        }
                    }
                }
                else{
                    foreach($servers->servers as $server) {
                        if($server->id == $serverid){
                            foreach ($server->apps as $app) {
                                if(empty($msg))
                                {
                                    $msg = "Applications for Server: " . $server->label . " are:\n";
                                }
                                $msg .= "ApplicationID:".$app->id." ApplicationLabel:".$app->label . "\n";
                            }
                        }
                    }
                }
                $this->send($this->getCurrentChannel(), null, $msg);
            }
            else{
                $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Note: Missing Arguments For The Following Operation");
            }
            break;
            case "hello":
            $username = $this->getUserNameFromUserId($this->getCurrentUser());
            $msg= "Hello @".$username."!\nFollowing are the commands that I can perform for now:
                    1. cloud hello
                    2. To List server type 'cloud getservers'
                    3. To List server applications type 'cloud getserverapplication (all|<serverid>)'
                    4. To Get server services running type 'cloud getservices <serverid>'
                    5. To Restart service type 'cloud restartservice (mysql|apache2|nginx|memcached|varnish|redis-server|php5-fpm|elasticsearch) <serverid>'
                    6. To Manage varnish type 'cloud varnish (purge|enable|disable) (all|<serverid>)'
                    7. To Manage server type 'cloud server (start|stop|restart|delete) <serverid>'
                    ";
            $this->send($this->getCurrentChannel(),null,$msg);
            break;
            case "restartserver":
            foreach($servers->servers as $server) {
                $result = $cw_api->service_varnish($server->id, 'purge');
                if(isset($result->response))
                {
                    $msg = "SERVER ->". $server->label.", Status cache purged";
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Varnish Purge");
                }else{
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Unable To Process your request");
                }
            }    
            break; 
            case "getservices":
            $serverid = isset($args[2]) ? $args[2] : '';
            $servers = $cw_api->get_services($serverid);
            $msg = null;
            foreach ($servers->services->status as $key => $value) {
                if($value === 0){
                    $value = "stopped";
                }
                if($value === 1){
                    $value = "running";
                }
                if(empty($msg)){
                    $msg = "\n".$key .": ".$value."\n";
                }else{
                    $msg .= $key .": ".$value."\n";
                }
            }
            $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
            break;
            case "restartservice":
            if(sizeof($args) == 4){
                $service = isset($args[2]) ? $args[2] : '';
                $serverid = isset($args[3]) ? $args[3] : '';
                $servers = $cw_api->manage_services($serverid,$service,"restart");
                $msg = "$service is restarted";
                $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
            }else{
                $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Note: All the parameters are required");
            }            
            break;
            case "varnish":
            if(sizeof($args) == 4){
                $service = isset($args[2]) ? $args[2] : '';
                $serverid = isset($args[3]) ? $args[3] : '';
                if($serverid === "all"){
                    $servers = $cw_api->get_servers();
                    foreach($servers->servers as $server) {
                        $result = $cw_api->service_varnish($server->id, $service);
                        if(isset($result->response))
                        {
                            $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Varnish $service of all server");
                        }else{
                            $this->send($this->getCurrentChannel(), $this->getCurrentUser(), var_dump($result));
                        }
                    } 
                }
                else{
                    $result = $cw_api->service_varnish($serverid, $service);
                    if(isset($result->response))
                    {
                        $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Varnish $service for server");
                    }else{
                        $this->send($this->getCurrentChannel(), $this->getCurrentUser(), var_dump($result));
                    }
                }
            }else{
                $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Not All the parameters are required");
            }            
            break;
            case "server":
            if(sizeof($args) == 4){
                $service = isset($args[2]) ? $args[2] : '';
                $serverid = isset($args[3]) ? $args[3] : '';
                $msg = "Server is $service";
                if($service == "delete"){
                    $servers = $cw_api->delete_server($serverid);
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
                }elseif($service == "start"){
                    $servers = $cw_api->start_server($serverid);
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
                }elseif($service == "stop"){
                    $servers = $cw_api->stop_server($serverid);
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
                }elseif($service == "restart"){
                    $servers = $cw_api->restart_server($serverid);
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
                }else{
                    $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "unknown command");
                }
            }else{
                $this->send($this->getCurrentChannel(), $this->getCurrentUser(), "Note: All the parameters are required");
            }            
            break;
            default:
            $msg= "valid commands are:
                    1. cloud hello
                    2. cloud getservers
                    3. cloud getserverapplication (all|<serverid>)
                    4. cloud getservices <serverid>
                    5. cloud restart (mysql|apache2|nginx|memcached|varnish|redis-server|php5-fpm|elasticsearch) <serverid>
                    6. cloud varnish (purge|enable|disable) (all|<serverid>)
                    7. cloud server (start|stop|restart|delete) <serverid>
                    ";
            $this->send($this->getCurrentChannel(), $this->getCurrentUser(), $msg);
            break;
        }
    }
    private function getArgs($message) {
        $args = array();
        if (isset($message['text'])) {
            $args = array_values(array_filter(explode(' ', $message['text'])));
        }
        $commandName = $this->getName();
        // Remove args which are before the command name
        $finalArgs = array();
        $remove = true;
        foreach ($args as $arg) {
            if ($commandName == $arg) {
                $remove = false;
            }
            if (!$remove) {
                $finalArgs[] = $arg;
            }
        }
        return $finalArgs;
    }
}
?>
